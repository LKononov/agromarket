﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AgroMarket.Models
{
    public class GetCountriesResult : Result
    {
        public List<CountriesList> Data { get; set; }
    }

    public class CountriesList
    {
        public int COUNTRY_ID { get; set; }
        public string COUNTRY_NAME { get; set; }
        public List<ProvincesList> PROVINCES { get; set; }
    }

    public class ProvincesList
    {
        public int PROVINCE_ID { get; set; }
        public string PROVINCE_NAME { get; set; }
    }
}