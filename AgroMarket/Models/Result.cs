﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AgroMarket.Models
{
    public class Result
    {
        public string Status { get; set; }
        public string Description { get; set; }
    }
}