﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AgroMarket.Models
{
    public class GetProductResult : Result
    {
        public List<GrouptList> Data { get; set; }
    }

    public class GrouptList
    {
        public int GROUP_ID { get; set; }
        public string GROUP_NAME { get; set; }
        public List<ProductList> PRODUCTS { get; set; }
    }

    public class ProductList
    {
        public int PRODUCT_ID { get; set; }
        public string PRODUCT_NAME { get; set; }
        public List<ClassifList> CLASSIFS { get; set; }
    }

    public class ClassifList
    {
        public int CLASSIF_ID { get; set; }
        public string CLASSIF_NAME { get; set; }
    }

    public class AddClassifList
    {
        public int CLASSIF_ID { get; set; }
        public int CLASSIF_VALUE { get; set; }
    }
}