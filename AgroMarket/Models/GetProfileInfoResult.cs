﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AgroMarket.Models
{
    public class GetProfileInfoResult : Result
    {
        public string AID { get; set; }
        public string NAME { get; set; }
        public string ABOUT_NATIVE { get; set; }
        public int COUNTRY_ID { get; set; }
        public int PROVINCE_ID { get; set; }
        public string COUNTRY_NAME { get; set; }
        public string PROVINCE_NAME { get; set; }
        public string ADDRESS { get; set; }
        public string SITE { get; set; }
        public string PHONE_1 { get; set; }
        public string PHONE_2 { get; set; }
        public string EMAIL { get; set; }
        public string EMAIL_1 { get; set; }
        public string EMAIL_2 { get; set; }
        public string SKYPE { get; set; }
        public string VIBER { get; set; }
    }
}