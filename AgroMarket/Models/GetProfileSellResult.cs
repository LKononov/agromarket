﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AgroMarket.Models
{
    public class GetProfileSellResult : Result
    {
        public List<GetProfileSellListResult> Data { get; set; }
    }

    public class GetProfileSellListResult
    {
        public Guid SID { get; set; }
        public int PRODUCT_ID { get; set; }
        public int GROUP_ID { get; set; }
        public string PRODUCT_NAME { get; set; }
        public string GROUP_NAME { get; set; }
        public int COUNTRY_ID { get; set; }
        public int PROVINCE_ID { get; set; }
        public string COUNTRY_NAME { get; set; }
        public string PROVINCE_NAME { get; set; }
        public string ADDRESS { get; set; }
        public bool PICKUP { get; set; }
        public int PRICE { get; set; }
        public bool NOCASH { get; set; }
        public string DESCRIPTION { get; set; }
        public string DATE { get; set; }
        public int VOLUME { get; set; }
        public List<GetProfileSellClassifResult> CLASSIF { get; set; }
    }

    public class GetProfileSellClassifResult
    {
        public int CLASSIF_ID { get; set; }
        public string CLASSIF_NAME { get; set; }
        public int VALUE { get; set; }
    }
}