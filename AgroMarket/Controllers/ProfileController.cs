﻿using AgroMarket.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AgroMarket.Controllers
{
    public class ProfileController : Controller
    {
        public JsonResult GetProfileInfo()
        {
            GetProfileInfoResult res = new GetProfileInfoResult();

            DataBaseDataContext OdContext = new DataBaseDataContext();

            List<W_USER> data = (from W_USER in OdContext.GetTable<W_USER>()
                                 where (W_USER.TEMP_KEY == new Guid(Convert.ToString(Request.Cookies["KEY"].Value)))
                                 select W_USER).ToList();

            if (data.Count == 0)
            {
                res.Status = "Error";
                res.Description = "Пользователь не найден";
            }
            else
            {
                res.Status = "Done";
                res.Description = "";

                res.AID = data[0].AID;
                res.NAME = data[0].NAME;
                res.ABOUT_NATIVE = data[0].ABOUT_NATIVE;
                res.COUNTRY_ID = data[0].COUNTRY_ID;
                res.PROVINCE_ID = data[0].PROVINCE_ID;
                res.ADDRESS = data[0].ADDRESS;
                res.SITE = data[0].SITE;
                res.PHONE_1 = data[0].PHONE_1;
                res.PHONE_2 = data[0].PHONE_2;
                res.EMAIL = data[0].EMAIL;
                res.EMAIL_1 = data[0].EMAIL_1;
                res.EMAIL_2 = data[0].EMAIL_2;
                res.SKYPE = data[0].SKYPE;
                res.VIBER = data[0].VIBER;

                //Вытягиваем название страны по COUNTRY_ID
                res.COUNTRY_NAME = (from W_COUNTRY in OdContext.GetTable<W_COUNTRY>()
                                    where (W_COUNTRY.COUNTRY_ID == res.COUNTRY_ID)
                                    select W_COUNTRY.COUNTRY_NAME).First();

                //Вытягиваем название области по PROVINCE_ID
                res.PROVINCE_NAME = (from W_PROVINCE in OdContext.GetTable<W_PROVINCE>()
                                     where (W_PROVINCE.PROVINCE_ID == res.PROVINCE_ID)
                                     select W_PROVINCE.PROVINCE_NAME).First();
            }

            return Json(res, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ChangeProfileinfo(string profile_name, string profile_about, int country_id, int province_id, string profile_address, string profile_site, string profile_phone1, string profile_phone2, string profile_email1, string profile_email2, string profile_skype, string profile_viber)
        {
            Result res = new Result();

            DataBaseDataContext OdContext = new DataBaseDataContext();

            W_USER user = (from W_USER in OdContext.GetTable<W_USER>()
                                     where (W_USER.TEMP_KEY == new Guid(Convert.ToString(Request.Cookies["KEY"].Value)))
                                     select W_USER).First();

            if (user == null)
            {
                res.Status = "Error";
                res.Description = "Пользователь не найден";
            }
            else
            {
                user.NAME = profile_name;
                user.ABOUT_NATIVE = profile_about;
                user.COUNTRY_ID = country_id;
                user.PROVINCE_ID = province_id;
                user.ADDRESS = profile_address;
                user.SITE = profile_site;
                user.PHONE_1 = profile_phone1;
                user.PHONE_2 = profile_phone2;
                user.EMAIL_1 = profile_email1;
                user.EMAIL_2 = profile_email2;
                user.SKYPE = profile_skype;
                user.VIBER = profile_viber;

                try
                {
                    OdContext.SubmitChanges();
                    res.Status = "Done";
                    res.Description = "";
                }
                catch (Exception e)
                {
                    res.Status = "Error";
                    res.Description = "Error: " + Convert.ToString(e.Message);
                }
            }

            return Json(res, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetSellInfo()
        {
            GetProfileSellResult res = new GetProfileSellResult();

            DataBaseDataContext OdContext = new DataBaseDataContext();

            List<W_USER> data_uid = (from W_USER in OdContext.GetTable<W_USER>()
                                     where (W_USER.TEMP_KEY == new Guid(Convert.ToString(Request.Cookies["KEY"].Value)))
                                     select W_USER).ToList();

            if (data_uid.Count == 0)
            {
                res.Status = "Error";
                res.Description = "Пользователь не найден";
            }
            else
            {
                //1. Загружаем список товаров
                Guid uid = data_uid[0].UID;
                List<W_SELL> data_sell = (from W_SELL in OdContext.GetTable<W_SELL>()
                                          where (W_SELL.UID == uid)
                                          select W_SELL).ToList();
                res.Data = new List<GetProfileSellListResult>();

                foreach (W_SELL val in data_sell)
                {
                    //Вытягиваем список товаров в виде полей
                    GetProfileSellListResult row = new GetProfileSellListResult();
                    row.SID         = val.SID;
                    row.PRODUCT_ID  = val.PRODUCT_ID;
                    row.GROUP_ID    = val.GROUP_ID;
                    row.COUNTRY_ID  = val.COUNTRY_ID;
                    row.PROVINCE_ID = val.PROVINCE_ID;
                    row.ADDRESS     = val.ADDRESS;
                    row.PICKUP      = val.PICKUP;
                    row.PRICE       = val.PRICE;
                    row.NOCASH      = val.NOCASH;
                    row.DESCRIPTION = val.DESCRIPTION;
                    row.DATE        = val.DATE.ToString("dd.MM.yyyy");
                    row.VOLUME      = val.VOLUME;

                    //Вытягиваем название товара по PRODUCT_ID
                    row.PRODUCT_NAME = (from W_PRODUCT in OdContext.GetTable<W_PRODUCT>()
                                        where (W_PRODUCT.PRODUCT_ID == row.PRODUCT_ID)
                                        select W_PRODUCT.PRODUCT_NAME).First();

                    //Вытягиваем название группы по GROUP_ID
                    row.GROUP_NAME = (from W_GROUP in OdContext.GetTable<W_GROUP>()
                                        where (W_GROUP.GROUP_ID == row.GROUP_ID)
                                        select W_GROUP.GROUP_NAME).First();

                    //Вытягиваем название страны по COUNTRY_ID
                    row.COUNTRY_NAME = (from W_COUNTRY in OdContext.GetTable<W_COUNTRY>()
                                        where (W_COUNTRY.COUNTRY_ID == row.COUNTRY_ID)
                                        select W_COUNTRY.COUNTRY_NAME).First();

                    //Вытягиваем название области по PROVINCE_ID
                    row.PROVINCE_NAME = (from W_PROVINCE in OdContext.GetTable<W_PROVINCE>()
                                        where (W_PROVINCE.PROVINCE_ID == row.PROVINCE_ID)
                                        select W_PROVINCE.PROVINCE_NAME).First();

                    //Вытягиваем список характеристик по PRODUCT_ID
                    List<W_CLASSIF> classif_list = (from W_CLASSIF in OdContext.GetTable<W_CLASSIF>()
                                              where (W_CLASSIF.PRODUCT_ID == row.PRODUCT_ID)
                                              select W_CLASSIF).ToList();

                    row.CLASSIF = new List<GetProfileSellClassifResult>();

                    foreach (W_CLASSIF val_classif in classif_list)
                    {
                        GetProfileSellClassifResult row_classif = new GetProfileSellClassifResult();

                        row_classif.CLASSIF_ID   = val_classif.CLASSIF_ID;
                        row_classif.CLASSIF_NAME = val_classif.CLASSIF_NAME;
                        row_classif.VALUE = 0;

                        row.CLASSIF.Add(row_classif);
                    }

                    //Вытягиваем список значений характеристик из SELL_CLASSIF_VALUES
                    List<W_SELL_CLASSIF_VALUE> data_classif = (from W_SELL_CLASSIF_VALUE in OdContext.GetTable<W_SELL_CLASSIF_VALUE>()
                                                               where (W_SELL_CLASSIF_VALUE.SID == row.SID)
                                                               select W_SELL_CLASSIF_VALUE).ToList();

                    foreach (W_SELL_CLASSIF_VALUE val_classif_value in data_classif)
                    {
                        for (int i = 0; i < row.CLASSIF.Count; i++)
                        {
                            if (row.CLASSIF[i].CLASSIF_ID == val_classif_value.CLASSIF_ID)
                            {
                                row.CLASSIF[i].VALUE = val_classif_value.VALUE;
                            }
                        }
                    }

                    res.Data.Add(row);
                }

                res.Status = "Done";
                res.Description = ""; 
            }

            return Json(res, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ChangeSellInfo(Guid sid, int type, int group, int product, int price, int volume, string description, int country, int province, string address, bool pickup, bool nocash, List<AddClassifList> classifs)
        {
            Result res = new Result();

            DataBaseDataContext OdContext = new DataBaseDataContext();

            List<W_USER> data_uid = (from W_USER in OdContext.GetTable<W_USER>()
                                     where (W_USER.TEMP_KEY == new Guid(Convert.ToString(Request.Cookies["KEY"].Value)))
                                     select W_USER).ToList();

            if (data_uid.Count == 0)
            {
                res.Status = "Error";
                res.Description = "Пользователь не найден";
            }
            else
            {
                W_SELL row = (from W_SELL in OdContext.GetTable<W_SELL>()
                              where (W_SELL.SID == sid)
                              select W_SELL).First();

                //row.PRICE = 1000;
                row.PRODUCT_ID = product;
                row.GROUP_ID = group;
                row.COUNTRY_ID = country;
                row.PROVINCE_ID = province;
                row.PRICE = price;
                row.DESCRIPTION = description;
                row.DATE = DateTime.Now;
                row.VOLUME = volume;
                row.ADDRESS = address;
                row.PICKUP = pickup;
                row.NOCASH = nocash;

                List<W_SELL_CLASSIF_VALUE> row_classif = (from W_SELL_CLASSIF_VALUE in OdContext.GetTable<W_SELL_CLASSIF_VALUE>()
                                                          where (W_SELL_CLASSIF_VALUE.SID == sid)
                                                          select W_SELL_CLASSIF_VALUE).ToList();

                foreach (W_SELL_CLASSIF_VALUE val_classifs in row_classif)
                {
                    for (int i = 0; i < classifs.Count; i++)
                    {
                        if (val_classifs.CLASSIF_ID == classifs[i].CLASSIF_ID)
                        {
                            val_classifs.VALUE = classifs[i].CLASSIF_VALUE;
                        }
                    }
                }

                try
                {
                    OdContext.SubmitChanges();
                    res.Status = "Done";
                    res.Description = "";
                }
                catch (Exception e)
                {
                    res.Status = "Error";
                    res.Description = "Error: " + Convert.ToString(e.Message);
                }
            }

            return Json(res, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteSellInfo(Guid sid)
        {
            Result res = new Result();

            DataBaseDataContext OdContext = new DataBaseDataContext();

            List<W_USER> data_uid = (from W_USER in OdContext.GetTable<W_USER>()
                                     where (W_USER.TEMP_KEY == new Guid(Convert.ToString(Request.Cookies["KEY"].Value)))
                                     select W_USER).ToList();

            if (data_uid.Count == 0)
            {
                res.Status = "Error";
                res.Description = "Пользователь не найден";
            }
            else
            {
                W_SELL row = (from W_SELL in OdContext.GetTable<W_SELL>()
                                where (W_SELL.SID == sid)
                                select W_SELL).First();

                OdContext.W_SELLs.DeleteOnSubmit(row);

                List<W_SELL_CLASSIF_VALUE> rows = (from W_SELL_CLASSIF_VALUE in OdContext.GetTable<W_SELL_CLASSIF_VALUE>()
                                                   where (W_SELL_CLASSIF_VALUE.SID == sid)
                                                   select W_SELL_CLASSIF_VALUE).ToList();

                foreach (W_SELL_CLASSIF_VALUE val in rows)
                {
                    OdContext.W_SELL_CLASSIF_VALUEs.DeleteOnSubmit(val);
                }
                
                try
                {
                    OdContext.SubmitChanges();
                    res.Status = "Done";
                    res.Description = "";
                }
                catch (Exception e)
                {
                    res.Status = "Error";
                    res.Description = "Error: " + Convert.ToString(e.Message);
                }
            }

            return Json(res, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetBuyInfo()
        {
            GetProfileBuyResult res = new GetProfileBuyResult();

            DataBaseDataContext OdContext = new DataBaseDataContext();

            List<W_USER> data_uid = (from W_USER in OdContext.GetTable<W_USER>()
                                     where (W_USER.TEMP_KEY == new Guid(Convert.ToString(Request.Cookies["KEY"].Value)))
                                     select W_USER).ToList();

            if (data_uid.Count == 0)
            {
                res.Status = "Error";
                res.Description = "Пользователь не найден";
            }
            else
            {
                //1. Загружаем список товаров
                Guid uid = data_uid[0].UID;
                List<W_BUY> data_buy = (from W_BUY in OdContext.GetTable<W_BUY>()
                                          where (W_BUY.UID == uid)
                                          select W_BUY).ToList();
                res.Data = new List<GetProfileBuyListResult>();

                foreach (W_BUY val in data_buy)
                {
                    //Вытягиваем список товаров в виде полей
                    GetProfileBuyListResult row = new GetProfileBuyListResult();
                    row.BID         = val.BID;
                    row.PRODUCT_ID = val.PRODUCT_ID;
                    row.GROUP_ID = val.GROUP_ID;
                    row.COUNTRY_ID = val.COUNTRY_ID;
                    row.PROVINCE_ID = val.PROVINCE_ID;
                    row.ADDRESS = val.ADDRESS;
                    row.PICKUP = val.PICKUP;
                    row.PRICE = val.PRICE;
                    row.NOCASH = val.NOCASH;
                    row.DESCRIPTION = val.DESCRIPTION;
                    row.DATE = val.DATE.ToString("dd.MM.yyyy");
                    row.VOLUME = val.VOLUME;

                    //Вытягиваем название товара по PRODUCT_ID
                    row.PRODUCT_NAME = (from W_PRODUCT in OdContext.GetTable<W_PRODUCT>()
                                        where (W_PRODUCT.PRODUCT_ID == row.PRODUCT_ID)
                                        select W_PRODUCT.PRODUCT_NAME).First();

                    //Вытягиваем название группы по GROUP_ID
                    row.GROUP_NAME = (from W_GROUP in OdContext.GetTable<W_GROUP>()
                                      where (W_GROUP.GROUP_ID == row.GROUP_ID)
                                      select W_GROUP.GROUP_NAME).First();

                    //Вытягиваем название страны по COUNTRY_ID
                    row.COUNTRY_NAME = (from W_COUNTRY in OdContext.GetTable<W_COUNTRY>()
                                        where (W_COUNTRY.COUNTRY_ID == row.COUNTRY_ID)
                                        select W_COUNTRY.COUNTRY_NAME).First();

                    //Вытягиваем название области по PROVINCE_ID
                    row.PROVINCE_NAME = (from W_PROVINCE in OdContext.GetTable<W_PROVINCE>()
                                         where (W_PROVINCE.PROVINCE_ID == row.PROVINCE_ID)
                                         select W_PROVINCE.PROVINCE_NAME).First();

                    //Вытягиваем список характеристик по PRODUCT_ID
                    List<W_CLASSIF> classif_list = (from W_CLASSIF in OdContext.GetTable<W_CLASSIF>()
                                                    where (W_CLASSIF.PRODUCT_ID == row.PRODUCT_ID)
                                                    select W_CLASSIF).ToList();

                    row.CLASSIF = new List<GetProfileBuyClassifResult>();

                    foreach (W_CLASSIF val_classif in classif_list)
                    {
                        GetProfileBuyClassifResult row_classif = new GetProfileBuyClassifResult();

                        row_classif.CLASSIF_ID = val_classif.CLASSIF_ID;
                        row_classif.CLASSIF_NAME = val_classif.CLASSIF_NAME;
                        row_classif.VALUE = 0;

                        row.CLASSIF.Add(row_classif);
                    }

                    //Вытягиваем список значений характеристик из SELL_CLASSIF_VALUES
                    List<W_BUY_CLASSIF_VALUE> data_classif = (from W_BUY_CLASSIF_VALUE in OdContext.GetTable<W_BUY_CLASSIF_VALUE>()
                                                               where (W_BUY_CLASSIF_VALUE.BID == row.BID)
                                                               select W_BUY_CLASSIF_VALUE).ToList();

                    foreach (W_BUY_CLASSIF_VALUE val_classif_value in data_classif)
                    {
                        for (int i = 0; i < row.CLASSIF.Count; i++)
                        {
                            if (row.CLASSIF[i].CLASSIF_ID == val_classif_value.CLASSIF_ID)
                            {
                                row.CLASSIF[i].VALUE = val_classif_value.VALUE;
                            }
                        }
                    }

                    res.Data.Add(row);
                }

                res.Status = "Done";
                res.Description = "";
            }

            return Json(res, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ChangeBuyInfo(Guid bid, int type, int group, int product, int price, int volume, string description, int country, int province, string address, bool pickup, bool nocash, List<AddClassifList> classifs)
        {
            Result res = new Result();

            DataBaseDataContext OdContext = new DataBaseDataContext();

            List<W_USER> data_uid = (from W_USER in OdContext.GetTable<W_USER>()
                                     where (W_USER.TEMP_KEY == new Guid(Convert.ToString(Request.Cookies["KEY"].Value)))
                                     select W_USER).ToList();

            if (data_uid.Count == 0)
            {
                res.Status = "Error";
                res.Description = "Пользователь не найден";
            }
            else
            {
                W_BUY row = (from W_BUY in OdContext.GetTable<W_BUY>()
                              where (W_BUY.BID == bid)
                              select W_BUY).First();

                //row.PRICE = 1000;
                row.PRODUCT_ID = product;
                row.GROUP_ID = group;
                row.COUNTRY_ID = country;
                row.PROVINCE_ID = province;
                row.PRICE = price;
                row.DESCRIPTION = description;
                row.DATE = DateTime.Now;
                row.VOLUME = volume;
                row.ADDRESS = address;
                row.PICKUP = pickup;
                row.NOCASH = nocash;

                List<W_BUY_CLASSIF_VALUE> row_classif = (from W_BUY_CLASSIF_VALUE in OdContext.GetTable<W_BUY_CLASSIF_VALUE>()
                                                          where (W_BUY_CLASSIF_VALUE.BID == bid)
                                                          select W_BUY_CLASSIF_VALUE).ToList();

                foreach (W_BUY_CLASSIF_VALUE val_classifs in row_classif)
                {
                    for (int i = 0; i < classifs.Count; i++)
                    {
                        if (val_classifs.CLASSIF_ID == classifs[i].CLASSIF_ID)
                        {
                            val_classifs.VALUE = classifs[i].CLASSIF_VALUE;
                        }
                    }
                }

                try
                {
                    OdContext.SubmitChanges();
                    res.Status = "Done";
                    res.Description = "";
                }
                catch (Exception e)
                {
                    res.Status = "Error";
                    res.Description = "Error: " + Convert.ToString(e.Message);
                }
            }

            return Json(res, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteBuyInfo(Guid bid)
        {
            Result res = new Result();

            DataBaseDataContext OdContext = new DataBaseDataContext();

            List<W_USER> data_uid = (from W_USER in OdContext.GetTable<W_USER>()
                                     where (W_USER.TEMP_KEY == new Guid(Convert.ToString(Request.Cookies["KEY"].Value)))
                                     select W_USER).ToList();

            if (data_uid.Count == 0)
            {
                res.Status = "Error";
                res.Description = "Пользователь не найден";
            }
            else
            {
                W_BUY row = (from W_BUY in OdContext.GetTable<W_BUY>()
                              where (W_BUY.BID == bid)
                              select W_BUY).First();

                OdContext.W_BUYs.DeleteOnSubmit(row);

                List<W_BUY_CLASSIF_VALUE> rows = (from W_BUY_CLASSIF_VALUE in OdContext.GetTable<W_BUY_CLASSIF_VALUE>()
                                                   where (W_BUY_CLASSIF_VALUE.BID == bid)
                                                   select W_BUY_CLASSIF_VALUE).ToList();

                foreach (W_BUY_CLASSIF_VALUE val in rows)
                {
                    OdContext.W_BUY_CLASSIF_VALUEs.DeleteOnSubmit(val);
                }

                try
                {
                    OdContext.SubmitChanges();
                    res.Status = "Done";
                    res.Description = "";
                }
                catch (Exception e)
                {
                    res.Status = "Error";
                    res.Description = "Error: " + Convert.ToString(e.Message);
                }
            }

            return Json(res, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Add(int type, int group, int product, int price, int volume, string description, int country, int province, string address, bool pickup, bool nocash, List<AddClassifList> classifs)
        {
            Result res = new Result();

            DataBaseDataContext OdContext = new DataBaseDataContext();

            List<W_USER> data_uid = (from W_USER in OdContext.GetTable<W_USER>()
                                     where (W_USER.TEMP_KEY == new Guid(Convert.ToString(Request.Cookies["KEY"].Value)))
                                     select W_USER).ToList();

            if (data_uid.Count == 0)
            {
                res.Status = "Error";
                res.Description = "Пользователь не найден";
            }
            else
            {
                Guid uid = data_uid[0].UID;

                switch(type)
                {
                    //Sell
                    case 1:
                        Guid new_sid = Guid.NewGuid();

                        W_SELL sell = new W_SELL();
                        sell.SID = new_sid;
                        sell.UID = uid;
                        sell.PRODUCT_ID = product;
                        sell.GROUP_ID = group;
                        sell.COUNTRY_ID = country;
                        sell.PROVINCE_ID = province;
                        sell.PRICE = price;
                        sell.DESCRIPTION = description;
                        sell.DATE = DateTime.Now;
                        sell.VOLUME = volume;
                        sell.ADDRESS = address;
                        sell.PICKUP = pickup;
                        sell.NOCASH = nocash;

                        foreach (AddClassifList val_classifs in classifs)
                        {
                            W_SELL_CLASSIF_VALUE classif = new W_SELL_CLASSIF_VALUE();
                            classif.SID = new_sid;
                            classif.CLASSIF_ID = val_classifs.CLASSIF_ID;
                            classif.VALUE = val_classifs.CLASSIF_VALUE;
                            OdContext.W_SELL_CLASSIF_VALUEs.InsertOnSubmit(classif);
                        }

                        OdContext.W_SELLs.InsertOnSubmit(sell);

                        OdContext.SubmitChanges();

                        res.Status = "Done";
                        res.Description = "";
                        break;
                    //Buy
                    case 2:
                        Guid new_bid = Guid.NewGuid();

                        W_BUY buy = new W_BUY();
                        buy.BID = new_bid;
                        buy.UID = uid;
                        buy.PRODUCT_ID = product;
                        buy.GROUP_ID = group;
                        buy.COUNTRY_ID = country;
                        buy.PROVINCE_ID = province;
                        buy.PRICE = price;
                        buy.DESCRIPTION = description;
                        buy.DATE = DateTime.Now;
                        buy.VOLUME = volume;
                        buy.ADDRESS = address;
                        buy.PICKUP = pickup;
                        buy.NOCASH = nocash;

                        foreach (AddClassifList val_classifs in classifs)
                        {
                            W_BUY_CLASSIF_VALUE classif = new W_BUY_CLASSIF_VALUE();
                            classif.BID = new_bid;
                            classif.CLASSIF_ID = val_classifs.CLASSIF_ID;
                            classif.VALUE = val_classifs.CLASSIF_VALUE;
                            OdContext.W_BUY_CLASSIF_VALUEs.InsertOnSubmit(classif);
                        }

                        OdContext.W_BUYs.InsertOnSubmit(buy);

                        OdContext.SubmitChanges();

                        res.Status = "Done";
                        res.Description = "";
                        break;
                    default:
                        res.Status = "Error";
                        res.Description = "Неверный тип";
                        break;
                }
            }
            return Json(res, JsonRequestBehavior.AllowGet);
        }
    }
}