﻿using AgroMarket.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AgroMarket.Controllers
{
    public class CompanyController : Controller
    {
        /*public JsonResult GetSellInfo(string aid)
        {
            GetProfileSellResult res = new GetProfileSellResult();

            DataBaseDataContext OdContext = new DataBaseDataContext();

            List<W_USER> data_uid = (from W_USER in OdContext.GetTable<W_USER>()
                                     where (W_USER.AID == aid)
                                     select W_USER).ToList();

            if (data_uid.Count == 0)
            {
                res.Status = "Error";
                res.Description = "Пользователь не найден";
            }
            else
            {
                Guid uid = data_uid[0].UID;

                List<W_SELL> data_sell = (from W_SELL in OdContext.GetTable<W_SELL>()
                                     where (W_SELL.UID == uid)
                                     select W_SELL).ToList();

                res.Data = new List<GetProfileSellListResult>();

                foreach (W_SELL val in data_sell)
                {
                    GetProfileSellListResult row = new GetProfileSellListResult();

                    row.SID         = val.SID;
                    row.PRODUCT_ID  = val.PRODUCT_ID;
                    row.PRICE       = val.PRICE;
                    row.DESCRIPTION = val.DESCRIPTION;
                    row.DATE        = val.DATE.ToString("dd.MM.yyyy");
                    row.VOLUME = val.VOLUME;

                    List<W_PRODUCT> data_product = (from W_PRODUCT in OdContext.GetTable<W_PRODUCT>()
                                         where (W_PRODUCT.PRODUCT_ID == row.PRODUCT_ID)
                                         select W_PRODUCT).ToList();

                    row.PRODUCT_NAME = data_product[0].PRODUCT_NAME;
                    row.GROUP_NAME = data_product[0].GROUP_NAME;
                    row.CLASSIF = new List<GetProfileSellClassifResult>();

                    foreach (W_PRODUCT val_classif in data_product)
                    {
                        GetProfileSellClassifResult row_classif = new GetProfileSellClassifResult();

                        row_classif.CLASSIF_ID = val_classif.CLASSIF_ID;
                        row_classif.CLASSIF_NAME = val_classif.CLASSIF_NAME;
                        row_classif.VALUE = null;
                        row.CLASSIF.Add(row_classif);
                    }

                    List<W_SELL_CLASSIF_VALUE> data_classif = (from W_SELL_CLASSIF_VALUE in OdContext.GetTable<W_SELL_CLASSIF_VALUE>()
                                                    where (W_SELL_CLASSIF_VALUE.SID == row.SID)
                                                    select W_SELL_CLASSIF_VALUE).ToList();

                    foreach (W_SELL_CLASSIF_VALUE val_classif_value in data_classif)
                    {
                        for(int i = 0; i< row.CLASSIF.Count; i++)
                        {
                            if(row.CLASSIF[i].CLASSIF_ID == val_classif_value.CLASSIF_ID)
                            {
                                row.CLASSIF[i].VALUE = val_classif_value.VALUE;
                            }
                        }
                    }

                    res.Data.Add(row);
                }

                res.Status = "Done";
                res.Description = "";
            }

            return Json(res, JsonRequestBehavior.AllowGet);
        }
        */

        public JsonResult GetProfileInfo(string aid)
        {
            GetProfileInfoResult res = new GetProfileInfoResult();

            DataBaseDataContext OdContext = new DataBaseDataContext();

            List<W_USER> data = (from W_USER in OdContext.GetTable<W_USER>()
                                 where (W_USER.AID == aid)
                                 select W_USER).ToList();

            if (data.Count == 0)
            {
                res.Status = "Error";
                res.Description = "Пользователь не найден";
            }
            else
            {
                res.Status = "Done";
                res.Description = "";
                res.NAME = data[0].NAME;
                res.ABOUT_NATIVE = data[0].ABOUT_NATIVE;
                res.ADDRESS = data[0].ADDRESS;
                res.SITE = data[0].SITE;
                res.PHONE_1 = data[0].PHONE_1;
                res.PHONE_2 = data[0].PHONE_2;
                res.EMAIL_1 = data[0].EMAIL_1;
                res.EMAIL_2 = data[0].EMAIL_2;
                res.SKYPE = data[0].SKYPE;
            }

            return Json(res, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetSellInfo(string aid)
        {
            GetProfileSellResult res = new GetProfileSellResult();

            DataBaseDataContext OdContext = new DataBaseDataContext();

            List<W_USER> data_uid = (from W_USER in OdContext.GetTable<W_USER>()
                                     where (W_USER.AID == aid)
                                     select W_USER).ToList();

            if (data_uid.Count == 0)
            {
                res.Status = "Error";
                res.Description = "Пользователь не найден";
            }
            else
            {
                //1. Загружаем список товаров
                Guid uid = data_uid[0].UID;
                List<W_SELL> data_sell = (from W_SELL in OdContext.GetTable<W_SELL>()
                                          where (W_SELL.UID == uid)
                                          select W_SELL).ToList();
                res.Data = new List<GetProfileSellListResult>();

                foreach (W_SELL val in data_sell)
                {
                    //Вытягиваем список товаров в виде полей
                    GetProfileSellListResult row = new GetProfileSellListResult();
                    row.SID = val.SID;
                    row.PRODUCT_ID = val.PRODUCT_ID;
                    row.GROUP_ID = val.GROUP_ID;
                    row.COUNTRY_ID = val.COUNTRY_ID;
                    row.PROVINCE_ID = val.PROVINCE_ID;
                    row.ADDRESS = val.ADDRESS;
                    row.PICKUP = val.PICKUP;
                    row.PRICE = val.PRICE;
                    row.NOCASH = val.NOCASH;
                    row.DESCRIPTION = val.DESCRIPTION;
                    row.DATE = val.DATE.ToString("dd.MM.yyyy");
                    row.VOLUME = val.VOLUME;

                    //Вытягиваем название товара по PRODUCT_ID
                    row.PRODUCT_NAME = (from W_PRODUCT in OdContext.GetTable<W_PRODUCT>()
                                        where (W_PRODUCT.PRODUCT_ID == row.PRODUCT_ID)
                                        select W_PRODUCT.PRODUCT_NAME).First();

                    //Вытягиваем название группы по GROUP_ID
                    row.GROUP_NAME = (from W_GROUP in OdContext.GetTable<W_GROUP>()
                                      where (W_GROUP.GROUP_ID == row.GROUP_ID)
                                      select W_GROUP.GROUP_NAME).First();

                    //Вытягиваем название страны по COUNTRY_ID
                    row.COUNTRY_NAME = (from W_COUNTRY in OdContext.GetTable<W_COUNTRY>()
                                        where (W_COUNTRY.COUNTRY_ID == row.COUNTRY_ID)
                                        select W_COUNTRY.COUNTRY_NAME).First();

                    //Вытягиваем название области по PROVINCE_ID
                    row.PROVINCE_NAME = (from W_PROVINCE in OdContext.GetTable<W_PROVINCE>()
                                         where (W_PROVINCE.PROVINCE_ID == row.PROVINCE_ID)
                                         select W_PROVINCE.PROVINCE_NAME).First();

                    //Вытягиваем список характеристик по PRODUCT_ID
                    List<W_CLASSIF> classif_list = (from W_CLASSIF in OdContext.GetTable<W_CLASSIF>()
                                                    where (W_CLASSIF.PRODUCT_ID == row.PRODUCT_ID)
                                                    select W_CLASSIF).ToList();

                    row.CLASSIF = new List<GetProfileSellClassifResult>();

                    foreach (W_CLASSIF val_classif in classif_list)
                    {
                        GetProfileSellClassifResult row_classif = new GetProfileSellClassifResult();

                        row_classif.CLASSIF_ID = val_classif.CLASSIF_ID;
                        row_classif.CLASSIF_NAME = val_classif.CLASSIF_NAME;
                        row_classif.VALUE = 0;

                        row.CLASSIF.Add(row_classif);
                    }

                    //Вытягиваем список значений характеристик из SELL_CLASSIF_VALUES
                    List<W_SELL_CLASSIF_VALUE> data_classif = (from W_SELL_CLASSIF_VALUE in OdContext.GetTable<W_SELL_CLASSIF_VALUE>()
                                                               where (W_SELL_CLASSIF_VALUE.SID == row.SID)
                                                               select W_SELL_CLASSIF_VALUE).ToList();

                    foreach (W_SELL_CLASSIF_VALUE val_classif_value in data_classif)
                    {
                        for (int i = 0; i < row.CLASSIF.Count; i++)
                        {
                            if (row.CLASSIF[i].CLASSIF_ID == val_classif_value.CLASSIF_ID)
                            {
                                row.CLASSIF[i].VALUE = val_classif_value.VALUE;
                            }
                        }
                    }

                    res.Data.Add(row);
                }

                res.Status = "Done";
                res.Description = "";
            }

            return Json(res, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetBuyInfo(string aid)
        {
            GetProfileBuyResult res = new GetProfileBuyResult();

            DataBaseDataContext OdContext = new DataBaseDataContext();

            List<W_USER> data_uid = (from W_USER in OdContext.GetTable<W_USER>()
                                     where (W_USER.AID == aid)
                                     select W_USER).ToList();

            if (data_uid.Count == 0)
            {
                res.Status = "Error";
                res.Description = "Пользователь не найден";
            }
            else
            {
                //1. Загружаем список товаров
                Guid uid = data_uid[0].UID;
                List<W_BUY> data_buy = (from W_BUY in OdContext.GetTable<W_BUY>()
                                        where (W_BUY.UID == uid)
                                        select W_BUY).ToList();
                res.Data = new List<GetProfileBuyListResult>();

                foreach (W_BUY val in data_buy)
                {
                    //Вытягиваем список товаров в виде полей
                    GetProfileBuyListResult row = new GetProfileBuyListResult();
                    row.BID = val.BID;
                    row.PRODUCT_ID = val.PRODUCT_ID;
                    row.GROUP_ID = val.GROUP_ID;
                    row.COUNTRY_ID = val.COUNTRY_ID;
                    row.PROVINCE_ID = val.PROVINCE_ID;
                    row.ADDRESS = val.ADDRESS;
                    row.PICKUP = val.PICKUP;
                    row.PRICE = val.PRICE;
                    row.NOCASH = val.NOCASH;
                    row.DESCRIPTION = val.DESCRIPTION;
                    row.DATE = val.DATE.ToString("dd.MM.yyyy");
                    row.VOLUME = val.VOLUME;

                    //Вытягиваем название товара по PRODUCT_ID
                    row.PRODUCT_NAME = (from W_PRODUCT in OdContext.GetTable<W_PRODUCT>()
                                        where (W_PRODUCT.PRODUCT_ID == row.PRODUCT_ID)
                                        select W_PRODUCT.PRODUCT_NAME).First();

                    //Вытягиваем название группы по GROUP_ID
                    row.GROUP_NAME = (from W_GROUP in OdContext.GetTable<W_GROUP>()
                                      where (W_GROUP.GROUP_ID == row.GROUP_ID)
                                      select W_GROUP.GROUP_NAME).First();

                    //Вытягиваем название страны по COUNTRY_ID
                    row.COUNTRY_NAME = (from W_COUNTRY in OdContext.GetTable<W_COUNTRY>()
                                        where (W_COUNTRY.COUNTRY_ID == row.COUNTRY_ID)
                                        select W_COUNTRY.COUNTRY_NAME).First();

                    //Вытягиваем название области по PROVINCE_ID
                    row.PROVINCE_NAME = (from W_PROVINCE in OdContext.GetTable<W_PROVINCE>()
                                         where (W_PROVINCE.PROVINCE_ID == row.PROVINCE_ID)
                                         select W_PROVINCE.PROVINCE_NAME).First();

                    //Вытягиваем список характеристик по PRODUCT_ID
                    List<W_CLASSIF> classif_list = (from W_CLASSIF in OdContext.GetTable<W_CLASSIF>()
                                                    where (W_CLASSIF.PRODUCT_ID == row.PRODUCT_ID)
                                                    select W_CLASSIF).ToList();

                    row.CLASSIF = new List<GetProfileBuyClassifResult>();

                    foreach (W_CLASSIF val_classif in classif_list)
                    {
                        GetProfileBuyClassifResult row_classif = new GetProfileBuyClassifResult();

                        row_classif.CLASSIF_ID = val_classif.CLASSIF_ID;
                        row_classif.CLASSIF_NAME = val_classif.CLASSIF_NAME;
                        row_classif.VALUE = 0;

                        row.CLASSIF.Add(row_classif);
                    }

                    //Вытягиваем список значений характеристик из SELL_CLASSIF_VALUES
                    List<W_BUY_CLASSIF_VALUE> data_classif = (from W_BUY_CLASSIF_VALUE in OdContext.GetTable<W_BUY_CLASSIF_VALUE>()
                                                              where (W_BUY_CLASSIF_VALUE.BID == row.BID)
                                                              select W_BUY_CLASSIF_VALUE).ToList();

                    foreach (W_BUY_CLASSIF_VALUE val_classif_value in data_classif)
                    {
                        for (int i = 0; i < row.CLASSIF.Count; i++)
                        {
                            if (row.CLASSIF[i].CLASSIF_ID == val_classif_value.CLASSIF_ID)
                            {
                                row.CLASSIF[i].VALUE = val_classif_value.VALUE;
                            }
                        }
                    }

                    res.Data.Add(row);
                }

                res.Status = "Done";
                res.Description = "";
            }

            return Json(res, JsonRequestBehavior.AllowGet);
        }
    }
}