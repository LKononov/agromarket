﻿using AgroMarket.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AgroMarket.Controllers
{
    public class ProductsController : Controller
    {
        // GET: Products
        public JsonResult GetProductList()
        {
            GetProductResult res = new GetProductResult();

            DataBaseDataContext OdContext = new DataBaseDataContext();

            //Вытягиеваем группы
            List<W_GROUP> groups = (from W_GROUP in OdContext.GetTable<W_GROUP>()
                                    select W_GROUP).ToList();
            res.Data = new List<GrouptList>();
            foreach (W_GROUP val_groups in groups)
            {
                //Вытягиваем список груп товаров
                GrouptList row_groups = new GrouptList();
                row_groups.GROUP_ID = val_groups.GROUP_ID;
                row_groups.GROUP_NAME = val_groups.GROUP_NAME;
                row_groups.PRODUCTS = new List<ProductList>();

                //Вытягиваем список товаров для кажой группы
                List<W_PRODUCT> products = (from W_PRODUCT in OdContext.GetTable<W_PRODUCT>()
                                          where (W_PRODUCT.GROUP_ID == row_groups.GROUP_ID)
                                          select W_PRODUCT).ToList();

                foreach (W_PRODUCT val_products in products)
                {
                    ProductList row_product = new ProductList();
                    row_product.PRODUCT_ID = val_products.PRODUCT_ID;
                    row_product.PRODUCT_NAME = val_products.PRODUCT_NAME;
                    row_product.CLASSIFS = new List<ClassifList>();

                    //Вытягиваем список характеристик для каждого товара
                    List<W_CLASSIF> classifs = (from W_CLASSIF in OdContext.GetTable<W_CLASSIF>()
                                                where (W_CLASSIF.PRODUCT_ID == row_product.PRODUCT_ID)
                                                select W_CLASSIF).ToList();

                    foreach (W_CLASSIF val_classifs in classifs)
                    {
                        ClassifList row_classif = new ClassifList();
                        row_classif.CLASSIF_ID = val_classifs.CLASSIF_ID;
                        row_classif.CLASSIF_NAME = val_classifs.CLASSIF_NAME;
                        row_product.CLASSIFS.Add(row_classif);
                    }

                    row_groups.PRODUCTS.Add(row_product);
                }

                res.Data.Add(row_groups);
            }
            res.Status = "Done";
            res.Description = "";

            return Json(res, JsonRequestBehavior.AllowGet);
        }

        /*public JsonResult GetProductClassif()
        {
            GetProductResult res = new GetProductResult();

            DataBaseDataContext OdContext = new DataBaseDataContext();

            //Вытягиеваем группы
            List<W_GROUP> groups = (from W_GROUP in OdContext.GetTable<W_GROUP>()
                                    select W_GROUP).ToList();
            res.Data = new List<GrouptList>();
            foreach (W_GROUP val_groups in groups)
            {
                //Вытягиваем список груп товаров
                GrouptList row_groups = new GrouptList();
                row_groups.GROUP_ID = val_groups.GROUP_ID;
                row_groups.GROUP_NAME = val_groups.GROUP_NAME;
                row_groups.PRODUCTS = new List<ProductList>();

                //Вытягиваем список товаров для кажой группы
                List<W_PRODUCT> products = (from W_PRODUCT in OdContext.GetTable<W_PRODUCT>()
                                            where (W_PRODUCT.GROUP_ID == row_groups.GROUP_ID)
                                            select W_PRODUCT).ToList();

                foreach (W_PRODUCT val_products in products)
                {
                    ProductList row_product = new ProductList();
                    row_product.PRODUCT_ID = val_products.PRODUCT_ID;
                    row_product.PRODUCT_NAME = val_products.PRODUCT_NAME;
                    row_product.CLASSIFS = new List<ClassifList>();

                    //Вытягиваем список характеристик для каждого товара
                    List<W_CLASSIF> classifs = (from W_CLASSIF in OdContext.GetTable<W_CLASSIF>()
                                                where (W_CLASSIF.PRODUCT_ID == row_product.PRODUCT_ID)
                                                select W_CLASSIF).ToList();

                    foreach (W_CLASSIF val_classifs in classifs)
                    {
                        ClassifList row_classif = new ClassifList();
                        row_classif.CLASSIF_ID = val_classifs.CLASSIF_ID;
                        row_classif.CLASSIF_NAME = val_classifs.CLASSIF_NAME;
                        row_product.CLASSIFS.Add(row_classif);
                    }

                    row_groups.PRODUCTS.Add(row_product);
                }

                res.Data.Add(row_groups);
            }
            res.Status = "Done";
            res.Description = "";

            return Json(res, JsonRequestBehavior.AllowGet);
        }*/
    }
}