﻿using AgroMarket.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Net.Mail;
using System.Net;

namespace AgroMarket.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index(string confirmdescription, Boolean? afterconfirmok = false, Boolean? afterconfirm = false)
        {
            if (Request.Cookies["KEY"] != null)
            {
                ViewBag.Layout = "~/Views/Shared/_SignLayout.cshtml";
            }
            else
            {
                ViewBag.Layout = "~/Views/Shared/_Layout.cshtml";
            }

            if(afterconfirm == true)
            {
                ViewBag.AfterConfirm = "true";
            }
            else
            {
                ViewBag.AfterConfirm = "false";
            }

            if (afterconfirmok == true)
            {
                ViewBag.AfterConfirmOK = "true";
            }
            else
            {
                ViewBag.AfterConfirmOK = "false";
            }

            ViewBag.ConfirmDescription = confirmdescription;
            ViewBag.Title = "Главная";
            return View();
        }

        public ActionResult Search()
        {
            if (Request.Cookies["KEY"] != null)
            {
                ViewBag.Layout = "~/Views/Shared/_SignLayout.cshtml";
            }
            else
            {
                ViewBag.Layout = "~/Views/Shared/_Layout.cshtml";
            }

            ViewBag.Title = "Обьявления";
            return View();
        }

        public ActionResult Firms()
        {
            if (Request.Cookies["KEY"] != null)
            {
                ViewBag.Layout = "~/Views/Shared/_SignLayout.cshtml";
            }
            else
            {
                ViewBag.Layout = "~/Views/Shared/_Layout.cshtml";
            }

            ViewBag.Title = "Фирмы";
            return View();
        }

        public ActionResult Company(string id)
        {
            if (Request.Cookies["KEY"] != null)
            {
                ViewBag.Layout = "~/Views/Shared/_SignLayout.cshtml";
            }
            else
            {
                ViewBag.Layout = "~/Views/Shared/_Layout.cshtml";
            }

            ViewBag.Title = "Фирма";
            ViewBag.AID = id;
            return View();
        }

        public ActionResult Help()
        {
            if (Request.Cookies["KEY"] != null)
            {
                ViewBag.Layout = "~/Views/Shared/_SignLayout.cshtml";
            }
            else
            {
                ViewBag.Layout = "~/Views/Shared/_Layout.cshtml";
            }

            ViewBag.Title = "Помощь";
            return View();
        }

        public ActionResult About()
        {
            if (Request.Cookies["KEY"] != null)
            {
                ViewBag.Layout = "~/Views/Shared/_SignLayout.cshtml";
            }
            else
            {
                ViewBag.Layout = "~/Views/Shared/_Layout.cshtml";
            }

            ViewBag.Title = "О нас";
            return View();
        }

        public ActionResult MyProfile()
        {
            if (Request.Cookies["KEY"] != null)
            {
                ViewBag.Layout = "~/Views/Shared/_SignLayout.cshtml";
            }
            else
            {
                ViewBag.Layout = "~/Views/Shared/_Layout.cshtml";
            }

            ViewBag.Title = "Кабинет";
            return View();
        }

        public ActionResult Registration()
        {
            if (Request.Cookies["KEY"] != null)
            {
                ViewBag.Layout = "~/Views/Shared/_SignLayout.cshtml";
            }
            else
            {
                ViewBag.Layout = "~/Views/Shared/_Layout.cshtml";
            }

            ViewBag.Title = "Регистрация";
            return View();
        }

        [HttpPost]
        public JsonResult Login(string email, string password)
        {
            Result res = new Result();

            DataBaseDataContext OdContext = new DataBaseDataContext();
            List<W_USER> data = (from W_USER in OdContext.GetTable<W_USER>()
                              where (W_USER.EMAIL == email) 
                              select W_USER).ToList();

            if(data.Count == 0)
            {
                res.Status = "Error";
                res.Description = "Пользователь не найден";
            }
            else
            {
                if(data[0].CONFIRM == false)
                {
                    res.Status = "Error";
                    res.Description = "Подтвердите регистрацию на почте";
                }
                else
                {
                    if (data[0].PASSWORD != password)
                    {
                        res.Status = "Error";
                        res.Description = "Неверный пароль";
                    }
                    else
                    {
                        Guid new_key = Guid.NewGuid();

                        foreach (W_USER val in data)
                        {
                            val.TEMP_KEY = new_key;
                        }

                        try
                        {
                            OdContext.SubmitChanges();
                            var cookie_key = new HttpCookie("KEY")
                            {
                                Name = "KEY",
                                Value = Convert.ToString(new_key),
                                Expires = DateTime.Now.AddMinutes(60),
                            };
                            Response.SetCookie(cookie_key);

                            res.Status = "Done";
                            res.Description = "Your key - " + Convert.ToString(new_key);
                        }
                        catch (Exception e)
                        {
                            res.Status = "Error";
                            res.Description = Convert.ToString(e.Message);
                        }
                    }
                } 
            }

            return Json(res);
        }

        public ActionResult Logout()
        {
            var cookie_key = new HttpCookie("KEY")
            {
                Name = "KEY",
                Value = null,
                Expires = DateTime.Now.AddMinutes(-1),
            };
            Response.SetCookie(cookie_key);

            return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        public JsonResult NewUSER(string email, string password, int country)
        {
            Result res = new Result();

            DataBaseDataContext OdContext = new DataBaseDataContext();
            List<W_USER> data = (from W_USER in OdContext.GetTable<W_USER>()
                                 where (W_USER.EMAIL == email)
                                 select W_USER).ToList();

            if (data.Count != 0)
            {
                res.Status = "Error";
                res.Description = "Пользователь с такой почтой уже существует";
            }
            else
            {
                try
                {
                    Guid new_key = Guid.NewGuid();
                    Guid new_uid = Guid.NewGuid();

                    W_USER objCourse = new W_USER();
                    objCourse.EMAIL = email;
                    objCourse.PASSWORD = password;
                    objCourse.UID = new_uid;
                    objCourse.COUNTRY_ID = country;

                    objCourse.CONFIRM = false;
                    objCourse.TEMP_KEY = new_key;

                    OdContext.W_USERs.InsertOnSubmit(objCourse);

                    OdContext.SubmitChanges();

                    //send email with new_key
                    SendEmail(email, "Подтверждение регистрации", "http://agro.agisgroup.eu/Home/ConfirmEmail?uid=" + new_uid+"&temp_key="+new_key);

                    res.Status = "Done";
                    res.Description = "Подтвердите регистрацию на почте";
                }
                catch (Exception e)
                {
                    res.Status = "Error";
                    res.Description = Convert.ToString(e.Message);
                }
            }

            return Json(res);
        }

        [HttpGet]
        public ActionResult ConfirmEmail(string uid, string temp_key)
        {
            DataBaseDataContext OdContext = new DataBaseDataContext();
            List<W_USER> data = (from W_USER in OdContext.GetTable<W_USER>()
                                 where (W_USER.UID == new Guid(uid))
                                 select W_USER).ToList();

            Boolean afterconfirm = true;
            Boolean afterconfirmok;
            string confirmdescription;

            if (data.Count == 0)
            {
                confirmdescription = "Пользователь не найден";
                afterconfirmok = false;
            }
            else
            {
                if (data[0].TEMP_KEY != new Guid(temp_key))
                {
                    confirmdescription = "Ключи не совпадают";
                    afterconfirmok = false;
                }
                else
                { 
                    foreach (W_USER val in data)
                    {
                        val.CONFIRM = true;
                    }
                    OdContext.SubmitChanges();
                    confirmdescription = "Почта успешно подтверждена";
                    afterconfirmok = true;
                }
            }
            return RedirectToAction("Index", "Home", new { confirmdescription = confirmdescription, afterconfirmok = afterconfirmok, afterconfirm = afterconfirm });
        }

        //[HttpPost]
        public JsonResult SendEmail(string to, string subject, string body)
        {
            Result res = new Result();

            try
            {
                var client = new SmtpClient("smtp.gmail.com", 587)//smtp server
                {
                    Credentials = new NetworkCredential("evgeniybutenko@gmail.com", "eb12081995"),//login, password
                    EnableSsl = true
                };
                client.Send("support@agisgroup.eu", to, subject, body);

                res.Status = "Done";
                res.Description = "gg"; 
            }
            catch(Exception e)
            {
                res.Status = "Error";
                res.Description = e.Message;
            }
            return Json(res);
        }
    }
}