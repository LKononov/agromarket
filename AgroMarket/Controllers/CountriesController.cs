﻿using AgroMarket.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AgroMarket.Controllers
{
    public class CountriesController : Controller
    {
        // GET: Countries
        public JsonResult GetCountryList()
        {
            GetCountriesResult res = new GetCountriesResult();

            DataBaseDataContext OdContext = new DataBaseDataContext();

            //Вытягиеваем группы
            List<W_COUNTRY> countries = (from W_COUNTRY in OdContext.GetTable<W_COUNTRY>()
                                    select W_COUNTRY).ToList();
            res.Data = new List<CountriesList>();
            foreach (W_COUNTRY val_countries in countries)
            {
                //Вытягиваем список стран
                CountriesList row_countries = new CountriesList();
                row_countries.COUNTRY_ID = val_countries.COUNTRY_ID;
                row_countries.COUNTRY_NAME = val_countries.COUNTRY_NAME;
                row_countries.PROVINCES = new List<ProvincesList>();

                //Вытягиваем список областей
                List<W_PROVINCE> provinces = (from W_PROVINCE in OdContext.GetTable<W_PROVINCE>()
                                            where (W_PROVINCE.COUNTRY_ID == row_countries.COUNTRY_ID)
                                            select W_PROVINCE).ToList();

                foreach (W_PROVINCE val_provinces in provinces)
                {
                    ProvincesList row_provinces = new ProvincesList();
                    row_provinces.PROVINCE_ID = val_provinces.PROVINCE_ID;
                    row_provinces.PROVINCE_NAME = val_provinces.PROVINCE_NAME;
                    row_countries.PROVINCES.Add(row_provinces);
                }

                res.Data.Add(row_countries);
            }
            res.Status = "Done";
            res.Description = "";

            return Json(res, JsonRequestBehavior.AllowGet);
        }
    }
}